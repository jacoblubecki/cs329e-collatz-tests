#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        a = "95 36\n"
        b, c = collatz_read(a)
        self.assertEqual(b, 95)
        self.assertEqual(c, 36)

    def test_read_3(self):
        d = "32 547\n"
        e, f = collatz_read(d)
        self.assertEqual(e, 32)
        self.assertEqual(f, 547)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        g = collatz_eval(1, 10)
        self.assertEqual(g, 20)

    def test_eval_2(self):
        h = collatz_eval(100, 200)
        self.assertEqual(h, 125)

    def test_eval_3(self):
        k = collatz_eval(201, 210)
        self.assertEqual(k, 89)

    def test_eval_4(self):
        l = collatz_eval(900, 1000)
        self.assertEqual(l, 174)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 12, 100, 119)
        self.assertEqual(w.getvalue(), "12 100 119\n")

    def test_print_2(self):
        v = StringIO()
        collatz_print(v, 555, 3765, 238)
        self.assertEqual(v.getvalue(), "555 3765 238\n")

    def test_print_3(self):
        x = StringIO()
        collatz_print(x, 45, 76, 116)
        self.assertEqual(x.getvalue(), "45 76 116\n")
    # -----
    # solve
    # -----

    def test_solve_1(self):
        run = StringIO("99 399\n4 32\n2 40\n957 94\n")
        woah = StringIO()
        collatz_solve(run, woah)
        self.assertEqual(woah.getvalue(), "99 399 144\n4 32 112\n2 40 112\n957 94 179\n")

    def test_solve_2(self):
        had = StringIO("64 10\n15 22\n1005 1010\n")
        mary = StringIO()
        collatz_solve(had, mary)
        self.assertEqual(mary.getvalue(),"64 10 113\n15 22 21\n1005 1010 112\n")

#----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
